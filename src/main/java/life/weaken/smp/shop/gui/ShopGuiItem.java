package life.weaken.smp.shop.gui;

import life.weaken.core.gui.guis.Gui;
import life.weaken.core.gui.items.GuiItem;
import life.weaken.smp.shop.ShopItem;
import life.weaken.smp.utils.ItemBuilder;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.PlayerInventory;

public class ShopGuiItem extends GuiItem {

    private final ShopItem item;
    private final Economy eco;

    public ShopGuiItem(Gui gui, Economy eco, ShopItem item) {
        super(gui, new ItemBuilder(item.getItem())
                .addLore(ChatColor.YELLOW + "Cost: $" + item.getCost())
                .create());
        this.item = item;
        this.eco = eco;
    }

    @Override
    public void click(InventoryClickEvent event) {
        super.click(event);

        Player player = (Player) event.getWhoClicked();
        if(eco.getBalance(player) < item.getCost()) {
            player.closeInventory();
            player.sendMessage(ChatColor.RED + "You don't have enough money to buy that!");
            return;
        }

        eco.withdrawPlayer(player, item.getCost());
        player.sendMessage(ChatColor.GREEN + "Purchase successful!");

        PlayerInventory inventory = player.getInventory();

        if(inventory.firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), item.getItem());
        } else {
            inventory.addItem(item.getItem());
        }

    }
}
