package life.weaken.smp.shop;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class ShopManager {

    private final List<ShopItem> shopList;
    private final Economy eco;

    @SuppressWarnings("ConstantConditions")
    public ShopManager(Plugin plugin, Economy eco) {
        this.eco = eco;
        shopList = new ArrayList<>();
        ConfigurationSection shopSection = plugin.getConfig().getConfigurationSection("shop");
        if(shopSection == null) return;

        for(String item : shopSection.getKeys(false)) {
            shopList.add(new ShopItem(item, shopSection.getConfigurationSection(item)));
        }
    }

    public List<ShopItem> getShopItems() {
        return shopList;
    }

    public Economy getEconomy() {
        return eco;
    }
}
