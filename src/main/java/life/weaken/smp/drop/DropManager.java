package life.weaken.smp.drop;

import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.*;
import org.bukkit.plugin.*;
import java.util.*;
import java.util.logging.Level;

public class DropManager {

    private final Plugin plugin;

    private final List<Drop> drops;
    private final List<Material> blocks;
    private int experience;

    public DropManager(Plugin plugin){
        this.plugin = plugin;

        drops = new ArrayList<>();
        blocks = new ArrayList<>();

        load(plugin.getConfig());
    }

    private void load(FileConfiguration config) {

        experience = config.getInt("drop.experience");

        ConfigurationSection dropsSection = config.getConfigurationSection("drop.drops");
        drops.clear();

        if(dropsSection == null) {
            plugin.getLogger().log(Level.WARNING, "No drops are defined in the config");
        }
        else {
            for (String string : dropsSection.getKeys(false)) {

                Material material = null;
                try {
                    material = Material.valueOf(string.toUpperCase());
                } catch(IllegalArgumentException e) {
                    plugin.getLogger().log(Level.WARNING, "Invalid material in drops: " + string.toUpperCase());
                }

                double chance = dropsSection.getDouble(string + ".chance");
                int max = dropsSection.getInt(string + ".max");
                int min = dropsSection.getInt(string + ".min");
                int yLevel = dropsSection.getInt(string + ".y-level");

                drops.add(new Drop(material, chance, min, max, yLevel));
            }
        }

        for(String string : config.getStringList("drop.blocks")) {
            try {
                blocks.add(Material.valueOf(string.toUpperCase()));
            } catch(IllegalArgumentException e) {
                plugin.getLogger().log(Level.WARNING, "Invalid material in drop blocks: " + string.toUpperCase());
            }
        }

    }

    public int getExperiencePerBlock() {
        return experience;
    }

    public List<Material> getBlocks() {
        return blocks;
    }

    public List<Drop> getAllDrops() {
        return drops;
    }

}