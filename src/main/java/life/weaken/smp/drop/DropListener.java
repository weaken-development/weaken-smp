package life.weaken.smp.drop;

import life.weaken.smp.utils.ItemBuilder;
import life.weaken.smp.utils.RandomUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public record DropListener(DropManager manager) implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        if(!manager.getBlocks().contains(event.getBlock().getType())) return;
        Block block = event.getBlock();
        Location location = block.getLocation();
        World world = location.getWorld();
        int y = event.getBlock().getY();

        if(world == null) return;

        ItemStack inUse = event.getPlayer().getInventory().getItemInMainHand();
        int fortune = inUse.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);

        for(Drop drop : manager.getAllDrops()) {
            if(y > drop.getyLevel() || RandomUtil.getRandDouble(0, 100) > drop.getChance() * (fortune + 1)) continue;
            int amount = RandomUtil.getRandInt(drop.getMin(), drop.getMax());

            world.spawn(location, ExperienceOrb.class).setExperience(manager.getExperiencePerBlock());
            world.dropItemNaturally(event.getBlock().getLocation(),
                    new ItemBuilder(drop.getMaterial())
                            .setAmount(amount)
                            .create());
        }
    }

}
