package life.weaken.smp.drop;

import org.bukkit.Material;

public class Drop {

    private final Material material;
    private final double chance;
    private final int min, max;
    private final int yLevel;

    public Drop(Material material, double chance, int min, int max, int yLevel) {
        this.material = material;
        this.chance = chance;
        this.min = min;
        this.max = max;
        this.yLevel = yLevel;
    }

    public double getChance() {
        return chance;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    public int getyLevel() {
        return yLevel;
    }

    public Material getMaterial() {
        return material;
    }

}

