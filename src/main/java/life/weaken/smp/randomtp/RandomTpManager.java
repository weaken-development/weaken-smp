package life.weaken.smp.randomtp;

import life.weaken.core.ServerDataStorage;
import life.weaken.smp.utils.RandomUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomTpManager {

    private final ServerDataStorage storage;
    private final List<Location> rtpBlocks;
    private final Plugin plugin;

    public RandomTpManager(Plugin plugin, ServerDataStorage storage) {
        this.storage = storage;
        this.plugin = plugin;
        rtpBlocks = new ArrayList<>();
        load();
    }

    private void load() {
        ConfigurationSection section = storage.getServerData().getConfigurationSection("random-tp");
        if(section == null) return;

        for(String string : section.getKeys(false)) {
            rtpBlocks.add(section.getLocation(string));
        }
    }

    private void save() {
        YamlConfiguration config = storage.getServerData();

        for(int i = 0; i < rtpBlocks.size(); i++) {
            config.set("random-tp." + i, rtpBlocks.get(i));
        }
    }

    public void shutdown() {
        save();
    }

    public void addBlock(Location location) {
        rtpBlocks.add(location);
    }

    public void removeBlock(Location location) {
        rtpBlocks.remove(location);
    }

    public List<Location> getBlocks() {
        return rtpBlocks;
    }

    public void teleport(Player player) {
        int x = RandomUtil.getRandInt(-2000, 2000);
        int z = RandomUtil.getRandInt(-2000, 2000);

        Block highest = player.getWorld().getHighestBlockAt(x, z);

        if(Arrays.asList(Material.WATER, Material.LAVA, Material.SEAGRASS, Material.TALL_SEAGRASS).contains(highest.getType())) {
            Bukkit.getScheduler().runTaskLater(plugin, () -> teleport(player), 5L);
            return;
        }

        Location loc = highest.getLocation().add(0.5, 1, 0.5);

        player.teleport(loc);
        player.sendMessage(ChatColor.GREEN + "Teleported to " + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ());
    }

}
