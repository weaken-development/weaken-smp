package life.weaken.smp.randomtp;

import org.bukkit.ChatColor;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public record RandomTpCommand(RandomTpManager manager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
            return true;
        }

        if(args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " help");
        }

        if(args[0].equalsIgnoreCase("add")) {
            Block block = player.getTargetBlockExact(6, FluidCollisionMode.NEVER);
            if(block == null) {
                sender.sendMessage(ChatColor.RED + "You are not looking at a block");
                return true;
            }

            if(manager.getBlocks().contains(block.getLocation())) {
                sender.sendMessage(ChatColor.RED + "That block is already a random tp block");
                return true;
            }

            manager.addBlock(block.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Added your target block as a random tp block");
        }

        else if(args[0].equalsIgnoreCase("remove")) {
            Block block = player.getTargetBlockExact(6, FluidCollisionMode.NEVER);
            if(block == null) {
                sender.sendMessage(ChatColor.RED + "You are not looking at a block");
                return true;
            }

            if(!manager.getBlocks().contains(block.getLocation())) {
                sender.sendMessage(ChatColor.RED + "That block is not a random tp block");
                return true;
            }

            manager.removeBlock(block.getLocation());
            sender.sendMessage(ChatColor.GREEN + "Removed your target block from random tp blocks");
        }

        else if(args[0].equalsIgnoreCase("clear")) {
            for(Location location : manager.getBlocks()) {
                manager.removeBlock(location);
            }
            sender.sendMessage(ChatColor.GREEN + "Removed all random tp blocks");
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }
}
