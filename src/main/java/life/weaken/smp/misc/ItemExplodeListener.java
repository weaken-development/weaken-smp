package life.weaken.smp.misc;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class ItemExplodeListener implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if(event.getCause() != EntityDamageEvent.DamageCause.ENTITY_EXPLOSION &&
                event.getCause() != EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) return;
        if(event.getEntityType() != EntityType.DROPPED_ITEM) return;
        event.setCancelled(true);
    }
}
