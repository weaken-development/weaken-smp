package life.weaken.smp.misc;

import life.weaken.smp.utils.ItemBuilder;
import life.weaken.smp.utils.RandomUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.data.type.Leaves;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ApplesListener implements Listener {

    private final int maxAmount;
    private final double chance;

    public ApplesListener(FileConfiguration config) {
        chance = config.getDouble("apples.chance");
        maxAmount = config.getInt("apples.max-amount");
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if(!(event.getBlock().getBlockData() instanceof Leaves)) return;
        if(RandomUtil.getRandDouble() > chance) return;

        Location loc = event.getBlock().getLocation();
        World world = loc.getWorld();

        if(world == null) return;

        world.dropItemNaturally(loc, new ItemBuilder(Material.APPLE)
                .setAmount(RandomUtil.getRandInt(1, maxAmount))
                .create());

    }


}
