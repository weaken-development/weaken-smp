package life.weaken.smp.misc;

import life.weaken.smp.utils.EnchantmentUtil;
import life.weaken.smp.utils.RandomUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VillagerListener implements Listener {

    private final HashMap<Enchantment, Integer> enchantmentMap;
    private final Integer EMERALD_AMOUNT;

    public VillagerListener(FileConfiguration config) {
        enchantmentMap = new HashMap<>();
        EMERALD_AMOUNT = config.getInt("villager.emeralds-for-enchant");
        ConfigurationSection section = config.getConfigurationSection("villager.allowed-enchants");
        if(section == null) return;

        for(String key : section.getKeys(false)) {
            Enchantment ench = EnchantmentUtil.getEnchantmentByKey(key);
            if(ench == null) continue;
            enchantmentMap.put(ench, section.getInt(key));
        }
    }

    @EventHandler
    public void onVillagerAqcquireTrade(VillagerAcquireTradeEvent event) {
        if(!(event.getEntity() instanceof Villager villager)) return;
        if(villager.getProfession() != Villager.Profession.LIBRARIAN) return;
        MerchantRecipe recipe = event.getRecipe();

        ItemStack result = recipe.getResult();
        if(!(result.getItemMeta() instanceof EnchantmentStorageMeta meta)) return;

        for(Enchantment enchantment : meta.getStoredEnchants().keySet()) meta.removeStoredEnchant(enchantment);
        Enchantment newEnch = (Enchantment) enchantmentMap.keySet().toArray()[RandomUtil.getRandInt(0, enchantmentMap.keySet().size() - 1)];

        meta.addStoredEnchant(newEnch, enchantmentMap.get(newEnch), false);
        result.setItemMeta(meta);

        List<ItemStack> ingredients = new ArrayList<>();
        for(ItemStack itemStack : recipe.getIngredients()) {
            if(itemStack.getType() == Material.EMERALD) itemStack.setAmount(EMERALD_AMOUNT);
            ingredients.add(itemStack.clone());
        }

        MerchantRecipe newRecipe = new MerchantRecipe(result, recipe.getUses(), recipe.getMaxUses(),
                recipe.hasExperienceReward(), recipe.getVillagerExperience(), recipe.getPriceMultiplier());

        newRecipe.setIngredients(ingredients);
        event.setRecipe(newRecipe);
    }

}
