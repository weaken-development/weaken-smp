package life.weaken.smp.reclaim;

import org.bukkit.configuration.ConfigurationSection;
import su.nightexpress.goldencrates.api.GoldenCratesAPI;
import su.nightexpress.goldencrates.manager.key.CrateKey;

import java.util.HashMap;
import java.util.Map;

public class Reclaim {

    private final int weight;
    private final String name;
    private final Map<CrateKey, Integer> keys;

    public Reclaim(String name, int weight, ConfigurationSection section) {
        this.weight = weight;
        this.name = name;
        keys = new HashMap<>();

        for(String string : section.getKeys(false)) {
            CrateKey key = GoldenCratesAPI.getKeyManager().getKeyById(string);
            if(key == null) continue;
            keys.put(key, section.getInt(string));
        }
    }

    public int getWeight() {
        return weight;
    }

    public Map<CrateKey, Integer> getKeys() {
        return keys;
    }

    public String getName() {
        return name;
    }
}
