package life.weaken.smp.utils;

import org.bukkit.enchantments.Enchantment;

public class EnchantmentUtil {

    public static Enchantment getEnchantmentByKey(String key) {
        for(Enchantment enchantment : Enchantment.values()) {
            if(key.equalsIgnoreCase(enchantment.getKey().getKey())) return enchantment;
        }
        return null;
    }

}
