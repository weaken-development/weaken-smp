package life.weaken.smp.utils;

import java.util.*;

public class RandomUtil {
    private static final Random rand = new Random();

    public static Double getRandDouble() {
        return rand.nextDouble();
    }

    public static Double getRandDouble(final double min, final double max) {
        return rand.nextDouble() * (max - min) + min;
    }

    public static boolean getChance(final double chance) {
        return chance >= 100.0 || chance >= getRandDouble(0.0, 100.0);
    }

    public static int getRandInt(final int min, final int max) {
        return rand.nextInt(max - min + 1) + min;
    }

}
